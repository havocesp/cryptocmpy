# -*- coding:utf-8 -*-
"""
CryptoCmpy

Python 3 "CryptoCompare" site API wrapper.
"""
from cryptocmpy.core import CryptoCmpy

__project__ = 'CryptoCmpy'
__package__ = 'cryptocmpy'

__author__ = 'Daniel J. Umpierrez'
__license__ = 'UNLICENSE'
__version__ = '0.1.2'

__description__ = __doc__

__site__ = 'https://github.com/havocesp/{}'.format(__package__)
__email__ = 'umpierrez@pm.me'

__keywords__ = ['altcoins', 'altcoin', 'exchange', 'mining', 'cryptocompare', 'crypto', 'compare', 'api', 'wrapper']

__all__ = ['__description__', '__author__', '__license__', '__version__', '__package__', '__project__', '__site__',
           '__email__', '__keywords__', 'CryptoCmpy']
